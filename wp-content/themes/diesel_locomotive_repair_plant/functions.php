<?php
/**
 * diesel_locomotive_repair_plant functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package diesel_locomotive_repair_plant
 */

if ( ! function_exists( 'diesel_locomotive_repair_plant_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function diesel_locomotive_repair_plant_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on diesel_locomotive_repair_plant, use a find and replace
		 * to change 'diesel_locomotive_repair_plant' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'diesel_locomotive_repair_plant', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'diesel_locomotive_repair_plant' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'diesel_locomotive_repair_plant_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'diesel_locomotive_repair_plant_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function diesel_locomotive_repair_plant_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'diesel_locomotive_repair_plant_content_width', 640 );
}
add_action( 'after_setup_theme', 'diesel_locomotive_repair_plant_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function diesel_locomotive_repair_plant_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'diesel_locomotive_repair_plant' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'diesel_locomotive_repair_plant' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'diesel_locomotive_repair_plant_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function diesel_locomotive_repair_plant_scripts() {
	wp_enqueue_style( 'diesel_locomotive_repair_plant-style', get_stylesheet_uri() );

	wp_enqueue_script( 'diesel_locomotive_repair_plant-navigation', get_template_directory_uri() . '/js/navigation.js', array('jquery'), '20151215', true );
	wp_enqueue_script( 'diesel_locomotive_repair_plant-slider', get_template_directory_uri() . '/js/slider.js', array('jquery'), '20151215', true );
	wp_enqueue_script( 'diesel_locomotive_repair_scripts', get_template_directory_uri() . '/js/scripts.js', array('jquery'), '20151215', true );

	wp_enqueue_script( 'diesel_locomotive_repair_plant-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

    wp_enqueue_style( 'header-style', get_theme_file_uri('/css/header.css') );
    wp_enqueue_style( 'page-style', get_theme_file_uri('/css/page.css') );
    wp_enqueue_style( 'single-style', get_theme_file_uri('/css/single-post.css') );
    wp_enqueue_style( 'footer-style', get_theme_file_uri('/css/footer.css') );
    if (is_admin()) {
        wp_enqueue_style( 'admin-style', get_theme_file_uri('/css/admin.css') );
    }
}
add_action( 'wp_enqueue_scripts', 'diesel_locomotive_repair_plant_scripts' );

/**
 * Enqueue scripts and styles.
 */
function diesel_locomotive_repair_plant_admin_scripts() {
    if (is_admin()) {
        wp_enqueue_style( 'admin-style', get_theme_file_uri('/css/admin.css') );
    }
}
add_action( 'admin_enqueue_scripts', 'diesel_locomotive_repair_plant_admin_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

function ztrz_display_posts() {
    global $wpdb;
    $total_pages = $offset = 0;
    $pag = 1;
    $table_name = $wpdb->base_prefix . 'posts';
    $number_of_posts = $wpdb->get_var('SELECT COUNT(ID) FROM `' . $table_name . '` WHERE `post_type` = "post" AND `post_status` = "publish"');
    if ($number_of_posts) {
        $total_pages = ceil($number_of_posts / 8);
    }

    if (isset($_GET['pag'])) {
        $pag = intval($_GET['pag']);
        $offset = $pag * 8 - 8;
    }

    $args = array(
        'post_type' => 'post',
        'numberposts' => 8,
        'offset' => $offset,
        'post_status' => 'publish',
    );

    $posts = get_posts($args);
    if ($posts) { ?>
        <div class="posts-section">
            <ul class="posts-list">
                <?php foreach ($posts as $post) {
                    $thumbnail = get_the_post_thumbnail($post->ID, 'large');
                    if (! $thumbnail) {
                        $thumbnail = '';
                    }
                    $post_date = new DateTime($post->post_date);
                    $_monthsList = array("01" => "января", "02" => "февраля",
                        "03" => "марта", "04" => "апреля", "05" => "мая", "06" => "июня",
                        "07" => "июля", "08" => "августа", "09" => "сентября",
                        "10" => "октября", "11" => "ноября", "12" => "декабря");
                    $date = $post_date->format('d');
                    $month = $post_date->format('m');
                    $month = $_monthsList[$month];
                    $year = $post_date->format('Y');

                    $end = strlen($post->post_content) > 700 ? '...' : ''; ?>

                    <li class="posts-item">
                        <div class="post_date"><?php echo $date . ' ' . $month . ' ' . $year; ?></div>
                        <div class="post_title"><a href="<?php echo $post->guid; ?>"><?php echo $post->post_title; ?></a></div>
                        <div class="clearfix">
                            <div class="post-image">
                                <?php echo $thumbnail; ?>
                            </div>
                            <div class="post_content"><?php echo mb_substr($post->post_content, 0, 700, 'UTF-8') . $end; ?>
                                <?php if ($end) { ?>
                                    <div class="post_reed-button">
                                        <a href="<?php echo $post->guid; ?>"><?php echo esc_html__( 'Читать всё', 'diesel_locomotive_repair_plant' );?></a>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </li>
                <?php } ?>
            </ul>
        </div>
    <?php } else {
        esc_html__( 'Новостей нет', 'diesel_locomotive_repair_plant' );
    }
    if (1 < $total_pages) { ?>
    <div class="pagination">
        <ul class="pagination-list">
            <?php for ($i = 1; $i <= $total_pages; $i++) {
                $class_active = $class_hidden = '';
                if ($pag > 8 && ($pag - 7) > $i ||
                    $pag < ($total_pages - 7) && ($pag + 7) < $i ) {
                    continue;
                }
                if($pag === $i) {
                    $class_active = 'active';
                } ?>
                <li class="pagination-item <?php echo $class_active; ?>"><a href="<?php echo site_url('/news/?pag=' . $i); ?>"><?php echo $i; ?></a></li>
            <?php } ?>
        </ul>
    </div>

    <?php }
}
add_shortcode('ztrz_posts', 'ztrz_display_posts');

require_once(get_theme_file_path('/inc/upload-price-list.php'));

add_action('save_post_post', 'add_attachment_if_need', 10, 3);
if (!function_exists('add_attachment_if_need')) {
    function add_attachment_if_need( $post_ID, $post, $update ) {
        global $wpdb;
        $table_name = $wpdb->base_prefix . 'posts';
        $thumbnail = get_post_thumbnail_id($post_ID);
        if (! $thumbnail) {
            $thumbnail_id = $wpdb->get_var('SELECT `ID` FROM `' . $table_name . '` WHERE `post_title` = "no_img" AND `post_type` = "attachment"');
            set_post_thumbnail( $post, $thumbnail_id );
        }
    }
}

add_action('wp_head', 'ztrz_add_metatags', 1);
if (!function_exists('ztrz_add_metatags')) {
    function ztrz_add_metatags() {
        global $post;
        $meta_description = '<meta name="description" content="ЗАПОРОЖСКИЙ ТЕПЛОВОЗОРЕМОНТНЫЙ ЗАВОД. Заходите на ' . $post->post_title . ' — ✓Опыт ✓Качество✓ Долгосрочное партнерство✓ Индивидуальный подход ✓Гибкие цены">';
        echo $meta_description;
    }
}

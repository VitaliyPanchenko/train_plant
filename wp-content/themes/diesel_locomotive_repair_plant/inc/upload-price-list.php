<?php
add_action('init', 'add_upload_page');

if (! function_exists('add_upload_page')) {
    function add_upload_page() {
        add_menu_page( __( 'Загрузить прайс услуг', 'diesel_locomotive_repair_plant_scripts' ), __( 'Прайс услуг', 'diesel_locomotive_repair_plant_scripts' ), 'manage_options', __( 'price_list', 'diesel_locomotive_repair_plant_scripts' ), 'upload_price_list_page', '' );
    }
}

if (! function_exists('upload_price_list_page')) {
    function upload_price_list_page() {
        $errors = array();
        $success = false;
        $uploaddir = get_theme_file_path('/price-list/');
        $uploaded = false;
        if (isset($_FILES['price_list'])) {
            $uploadfile = $uploaddir . basename($_FILES['price_list']['name']);

            if ('xlsx' === pathinfo($uploadfile)['extension']) {

                if (file_exists($uploaddir)) {
                    foreach (glob($uploaddir . '*') as $file) {
                        unlink($file);
                    }
                }

                $uploaded = move_uploaded_file($_FILES['price_list']['tmp_name'], $uploadfile);
                $success = true;
            } else {
                $errors[] = __( 'Неверный формат файла', 'diesel_locomotive_repair_plant_scripts' );
            }
        }
        ?>
        <div class="wrapper">
            <div class="page-title">
                <p>
                    <?php echo __( 'Загрузить прайс услуг', 'diesel_locomotive_repair_plant_scripts' );?>
                </p>
            </div>
            <?php if ($errors) {
                foreach ($errors as $error) { ?>
                    <p class="error"><?php echo $error ?></p>
                <?php }
            }

            if ($success) { ?>
                <p class="success"><?php echo __( 'Файл успешно загружен', 'diesel_locomotive_repair_plant_scripts' ); ?></p>
            <?php } ?>
            <div class="current-price-list">
                <?php $price_list_dir = scandir($uploaddir);
                if (isset($price_list_dir[2])) { ?>
                    <span class="current-price-list__title"><?php echo __( 'Текущий прайс лист:', 'diesel_locomotive_repair_plant_scripts' );?></span>
                    <span class="current-price-list__file"><?php echo $price_list_dir[2] ?></span>
                <?php }
                ?>
            </div>
            <div class="form-wrapper">
                <form method="POST" enctype="multipart/form-data">
                    <label for="price-list">
                        <input type="file" id="price-list" name="price_list">
                    </label>
                    <p class="price-list-notice"><?php echo __( 'Файл должен быть в формате exel (.xlsx)', 'diesel_locomotive_repair_plant_scripts' );?></p>
                    <div class="submit-button">
                        <input type="submit">
                    </div>
                </form>
            </div>
        </div>
    <?php }
}

add_action('init', 'add_price_list_file_name');
if (! function_exists('add_price_list_file_name')) {
    function add_price_list_file_name() {
        if ( '/spares/' !== $_SERVER['REQUEST_URI']) {
            return;
        }
        $uploaddir = get_theme_file_path('/price-list/');
        if (file_exists($uploaddir)) {
            $file = preg_replace('#/.*/#', '', glob($uploaddir . '*')[0]);
            $link = get_theme_file_uri('/price-list/') . $file;
        } ?>
        <script>
            var priceListFile = '<?php echo $link; ?>';
        </script>
    <?php }
}

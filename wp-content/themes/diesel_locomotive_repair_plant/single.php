<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package diesel_locomotive_repair_plant
 */

get_header();
?>

    <div class="main-banner single-post"></div>
	<div id="primary-post" class="content-area">
		<main id="main-post" class="site-main">
            <section class="page-title-section">
                <div class="kc-title-wrap">
                    <h4 class="kc_title subtitle white">
                        Запорожский<b>тепловозоремонтный</b>завод
                    </h4>
                </div>
                <div class="kc-title-wrap ">
                    <h1 class="kc_title ztrz-page-title">Новости</h1>
                </div>
            </section>
            <section class="single-news">
                <div class="container">

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/post-content', get_post_type() );

			// If comments are open or we have at least one comment, load up the comment template.
//			if ( comments_open() || get_comments_number() ) :
//				comments_template();
//			endif;

		endwhile; // End of the loop.
		?>
                </div>
            </section>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();

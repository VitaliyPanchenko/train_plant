<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package diesel_locomotive_repair_plant
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'diesel_locomotive_repair_plant' ); ?></a>

	<header id="masthead" class="site-header">
		<div class="header-container">
			<div class="site-branding">
				<?php
				the_custom_logo();
				if ( is_front_page() && is_home() ) :
					?>
					<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
					<?php
				else :
					?>
					<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
					<?php
				endif;
				$diesel_locomotive_repair_plant_description = get_bloginfo( 'description', 'display' );
				if ( $diesel_locomotive_repair_plant_description || is_customize_preview() ) :
					?>
					<p class="site-description"><?php echo $diesel_locomotive_repair_plant_description; /* WPCS: xss ok. */ ?></p>
				<?php endif; ?>
			</div><!-- .site-branding -->

			<?php $img_dir = get_theme_file_uri('/img/'); ?>

			<div class="site-brand">
				<a href="/"><img src="<?php echo $img_dir . 'logo.png'; ?>" alt="<?php esc_html_e( 'Тепловозоремонтый завод', 'diesel_locomotive_repair_plant' ); ?>"></a>
			</div>

			<nav id="site-navigation" class="main-navigation">
				<div class="social-icons">
					<div class="social-icons__item facebook-icon">
						<a href="https://www.facebook.com/ztrzua/" target="_blank"><img src="<?php echo $img_dir . 'facebook.svg'; ?>" alt="Facebook" title="<?php __('Перейти на страницу Facebook'); ?>"></a>
					</div>
				</div>
				<span class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">&#9776;</span>
				<?php
				wp_nav_menu( array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'primary-menu',
				) );
				?>
			</nav><!-- #site-navigation -->
		</div>
	</header><!-- #masthead -->

	<div id="content" class="site-content">

<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package diesel_locomotive_repair_plant
 */

get_header(); ?>

    <div class="main-banner not-found"></div>

    <div id="primary" class="content-area">
        <main id="main" class="site-main">

            <section id="top-banner-block-not-found" class="kc-elm kc-css-431758 kc_row">
                <div class="kc-row-container  kc-container">
                    <div class="kc-wrap-columns">
                        <div class="kc-elm kc-css-678601 kc_col-sm-12 kc_column kc_col-sm-12">
                            <div class="kc-col-container">
                                <div class="kc-elm kc-css-987421 kc-title-wrap ">
                                    <h1 class="title">404</h1>
                                </div>
                                <div class="kc-elm kc-css-550682 kc_shortcode kc_single_image train"
                                     style="left: 1518px;">

                                    <img src="http://ztrz.webphpdev.site/wp-content/uploads/2020/03/locomotive.png"
                                         class="" alt=""></div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="error-404 not-found">
                <header class="error-404__page-header">
                    <h2 class="error-404__page-title"><?php esc_html_e('Страница не найдена!', 'diesel_locomotive_repair_plant'); ?></h2>
                </header><!-- .page-header -->

                <div class="error-404__page-content">
                    <p><?php esc_html_e('Данная страница не найдена. Попробуйте выбрать другой пункт меню', 'diesel_locomotive_repair_plant'); ?></p>
                </div><!-- .page-content -->
            </section><!-- .error-404 -->

        </main><!-- #main -->
    </div><!-- #primary -->

<?php
get_footer();

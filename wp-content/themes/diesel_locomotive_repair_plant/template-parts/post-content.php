<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package diesel_locomotive_repair_plant
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <?php global $post;
    $post_date = new DateTime($post->post_date);
    $_monthsList = array("01" => "января", "02" => "февраля",
        "03" => "марта", "04" => "апреля", "05" => "мая", "06" => "июня",
        "07" => "июля", "08" => "августа", "09" => "сентября",
        "10" => "октября", "11" => "ноября", "12" => "декабря");
    $date = $post_date->format('d');
    $month = $post_date->format('m');
    $month = $_monthsList[$month];
    $year = $post_date->format('Y');

    diesel_locomotive_repair_plant_post_thumbnail(); ?>

    <div class="post_date">
        <p class="post_date__date"><?php echo $date; ?></p>
        <p class="post_date__month"><?php echo $month; ?></p>
        <p class="post_date__year"><?php echo $year; ?></p>
    </div>
    <div class="post-title"><?php echo get_the_title(); ?></div>
    <div class="entry-content">
        <?php the_content( sprintf(
            wp_kses(
            /* translators: %s: Name of current post. Only visible to screen readers */
                __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'diesel_locomotive_repair_plant' ),
                array(
                    'span' => array(
                        'class' => array(),
                    ),
                )
            ),
            get_the_title()
        ) );

        wp_link_pages( array(
            'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'diesel_locomotive_repair_plant' ),
            'after'  => '</div>',
        ) );
        ?>
    </div><!-- .entry-content -->
    <div class="return-button">
        <a href="<?php echo site_url('/news') ?>" alt="">Все новости</a>
    </div>
</article><!-- #post-<?php the_ID(); ?> -->

<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package diesel_locomotive_repair_plant
 */

?>

	</div>
	</div><!-- #content -->
<?php $img_dir = get_theme_file_uri('/img/'); ?>

	<footer id="colophon" class="site-footer">
        <div class="footer-top">
            <div class="footer-container footer-flex-container">
                <div class="footer-top__column footer-banner">
                    <img src="<?php echo $img_dir . 'logo_copy.png' ?>" alt="<?php esc_html_e( 'Тепловозоремонтый завод', 'diesel_locomotive_repair_plant' ); ?>"/>
                </div>
                <div class="footer-top__column">
                    <div class="column-title phone"><?php esc_html_e( 'Телефоны', 'diesel_locomotive_repair_plant' ); ?></div>
                    <p><?php esc_html_e( 'Руководитель, ремонт и приобретение тепловозов:', 'diesel_locomotive_repair_plant' ); ?> +38 095 500 10 02</p>
                    <p><?php esc_html_e( 'Технический отдел:', 'diesel_locomotive_repair_plant' ); ?> +38 095 449 51 77</p>
                    <p><?php esc_html_e( 'Отдел материально-технического снабжения:', 'diesel_locomotive_repair_plant' ); ?> +38 098 912 60 47, +38 093 58 466 58, +38 095 574 00 44 (Viber, WhatsUP, Telegramm)</p>
                    <p><?php esc_html_e( 'Начальник отдела материально-технического снабжения:', 'diesel_locomotive_repair_plant' ); ?> +38099 25 603 45</p>
                </div>
                <div class="footer-top__column">
                    <div class="column-title address"><?php esc_html_e( 'Адрес', 'diesel_locomotive_repair_plant' ); ?></div>
                    <p style="text-decoration: underline"><?php esc_html_e( 'Юридический адрес:', 'diesel_locomotive_repair_plant' ); ?></p>
                    <p><?php esc_html_e( 'Украина, Запорожская область, Запорожье, 69097', 'diesel_locomotive_repair_plant' ); ?></p>
                    <p><?php esc_html_e( 'ул. Демократическая, 73', 'diesel_locomotive_repair_plant' ); ?></p>
                    <p style="text-decoration: underline"><?php esc_html_e( 'Ремонтная база:', 'diesel_locomotive_repair_plant' ); ?></p>
                    <p><?php esc_html_e( 'Украина, Запорожская область, Запорожье, 69000', 'diesel_locomotive_repair_plant' ); ?></p>
                    <p><?php esc_html_e( 'Вологодский переулок, 30', 'diesel_locomotive_repair_plant' ); ?></p>
                </div>
                <div class="footer-top__column">
                    <div  class="column-title email"><?php esc_html_e( 'Почта', 'diesel_locomotive_repair_plant' ); ?></div>
                    <p>email: locomotive@ztrz.com.ua</p>
                    <p>Site: ztrz.com.ua</p>
                    <p>Skype: Perekrestv</p>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="footer-container">
                <div class="footer-bottom__menu footer-menu">
                    <ul class="footer-flex-container footer-menu__list">
                        <li class="footer-menu__item footer-bottom__company">
                            <p class="footer-bottom__who"><a href="/"><?php esc_html_e( 'Кто мы?', 'diesel_locomotive_repair_plant' ); ?></a></p>
                            <p class="footer-bottom__what-we-do"><b><?php esc_html_e( 'Запорожский Тепловозоремонтный Завод ', 'diesel_locomotive_repair_plant' ); ?> </b> <?php esc_html_e( 'производит капитальные ремонты маневровых тепловозов, а также узлов тепловозов.', 'diesel_locomotive_repair_plant' ); ?> </p>
                        </li>
                        <li class="footer-menu__item"><a><?php esc_html_e( 'Ремонтные услуги и модернизация', 'diesel_locomotive_repair_plant' ); ?></a>
                            <ul class="footer-menu__sub-list">
                                <li class="footer-menu__sub-item"><a href="/maintenance/"><?php esc_html_e( 'Текущий ремонт', 'diesel_locomotive_repair_plant' ); ?></a></li>
                                <li class="footer-menu__sub-item"><a href="/overhaul/"><?php esc_html_e( 'Капитальный ремонт', 'diesel_locomotive_repair_plant' ); ?></a></li>
                                <li class="footer-menu__sub-item"><a href="/motorization-modernization/"><?php esc_html_e( 'Моторизация, модернизация', 'diesel_locomotive_repair_plant' ); ?></a></li>
                            </ul>
                        </li>
                        <li class="footer-menu__item"><a><?php esc_html_e( 'Реализация оборудования', 'diesel_locomotive_repair_plant' ); ?></a>
                            <ul class="footer-menu__sub-list">
                                <li class="footer-menu__sub-item"><a href="/spares/"><?php esc_html_e( 'Запчасти', 'diesel_locomotive_repair_plant' ); ?></a></li>
                                <li class="footer-menu__sub-item"><a href="/rti/"><?php esc_html_e( 'РТИ', 'diesel_locomotive_repair_plant' ); ?></a></li>
                                <li class="footer-menu__sub-item"><a href="/aggregates-after-repair/"><?php esc_html_e( 'Узлы, агрегаты после КР', 'diesel_locomotive_repair_plant' ); ?></a></li>
                                <li class="footer-menu__sub-item"><a href="/locomotives-after-repair/"><?php esc_html_e( 'Тепловозы после КР', 'diesel_locomotive_repair_plant' ); ?></a></li>
                            </ul>
                        </li>
                        <li class="footer-menu__item"><a href="/news/"><?php esc_html_e( 'Новости', 'diesel_locomotive_repair_plant' ); ?></a>
                            <ul class="footer-menu__sub-list">
                                <li class="footer-menu__sub-item"><a href="/news/"><?php esc_html_e( 'Текущие новости', 'diesel_locomotive_repair_plant' ); ?></a></li>
                            </ul>
                        </li>
                        <li class="footer-menu__item"><a><?php esc_html_e( 'О компании', 'diesel_locomotive_repair_plant' ); ?></a>
                            <ul class="footer-menu__sub-list">
                                <li class="footer-menu__sub-item"><a href="/certificates/"><?php esc_html_e( 'Сертификаты', 'diesel_locomotive_repair_plant' ); ?></a></li>
                                <li class="footer-menu__sub-item"><a href="/gallery/"><?php esc_html_e( 'Фотогалерея', 'diesel_locomotive_repair_plant' ); ?></a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="social-icons footer-social">
                    <div class="social-icons__item facebook-icon">
                        <a href="https://www.facebook.com/ztrzua/" target="_blank"><img src="<?php echo $img_dir . 'facebook.svg'; ?>" alt="Facebook" title="<?php __('Перейти на страницу Facebook'); ?>"></a>
                    </div>
                    <!--<div class="social-icons__item youtube-icon">
                        <a href="#" target="_blank"><img src="<?php echo $img_dir . 'youtube.svg'; ?>" alt="Youtube" title="<?php __('Перейти на канал Youtube'); ?>"></a>
                    </div>-->
                </div>
            </div>
        </div>
        <div class="footer-designed">
            <p>© 2020   Designed by It Master-Soft</p>
        </div>

	</footer>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>

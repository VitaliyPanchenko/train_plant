( function($) {
    $(document).on('ready', function () {
        let headerHeight = $('#masthead').height();
        $('#primary').css('margin-top', headerHeight);
        $('#primary-post').css('margin-top', headerHeight);

        if (-1 !== navigator.userAgent.indexOf('Firefox')
            && '/' ===  window.location.pathname
            && $(window).width() > 1200
        ) {
            $('.main-banner.home').height(830);
        }

        let train = $('.train');
        if (train.length) {
            let offsetLeft = parseInt( train.offset().left );
            let windowWidth = $(window).width();
            let cssLeft = parseInt( train.css('left').replace('px', '') );
            let dif = offsetLeft - cssLeft;
            windowWidth = windowWidth - dif;

            addCoordinate(train, cssLeft, windowWidth);
        }

        $('.upload-price-list a').each(function () {
            $(this).attr('href', priceListFile);

        });
    });

    function addCoordinate(selector, currentPosition, windowWidth) {
        currentPosition++;
        setTimeout(function () {
            if (currentPosition < windowWidth) {
                selector.css('left', currentPosition);
                addCoordinate(selector, currentPosition, windowWidth);
            }

        }, 1, selector, currentPosition, windowWidth);
    }
} )( jQuery );
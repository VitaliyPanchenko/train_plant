/**
 * File navigation.js.
 *
 * Handles toggling the navigation menu for small screens and enables TAB key
 * navigation support for dropdown menus.
 */
( function($) {
	$(document).ready(function() {
		$('body').on('click', '.menu-toggle', function () {
			$('#primary-menu').toggleClass('active');
		});

		$('body').on('mouseenter touchstart', '#primary-menu li', function() {
			$(this).find('.sub-menu').addClass('visible');
		});

		$('body').on('mouseleave', '#primary-menu li', function() {
			$(this).find('.sub-menu').removeClass('visible');
		});

		$('a[href="' + window.location.pathname + '"]').addClass('current-location');
		$('.menu a').each(function() {
			if (-1 !== window.location.href.indexOf($(this).attr('href'))) {
				$(this).addClass('current-location');
			}
		});
		let currentLocationElement = $('.current-location');
		if (currentLocationElement.parent().hasClass('footer-menu__sub-item') || currentLocationElement.parents('.sub-menu').length) {
			currentLocationElement.parent().parent().prev('a').addClass('current-location')
		}

		$('body').on('click touchstart', '.anchor', function() {
			$('.sub-menu .current-location').removeClass('current-location');
			$(this).find('a').addClass('current-location');
		});

		$('#primary-menu > .menu-item > a').each(function () {
			if (undefined !== $(this).attr('href')
				&& ( -1 !== $(this).attr('href').indexOf('services')
				|| -1 !== $(this).attr('href').indexOf('implementation')
				|| (window.location.origin + '/') === $(this).attr('href') )
			) {
				$(this).attr('href', '');
			}
		})
	});

} )( jQuery );

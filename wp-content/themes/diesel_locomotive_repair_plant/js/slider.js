( function($) {
    $(document).on('ready', function () {
       let entrtyMeta = $('.kc-entry_meta');
       let dateBlock = entrtyMeta.find('.date time');
        dateBlock.each(function () {
            let time = $(this).html();
            time = time.replace(',', '');
            time = time.split(' ');
            let html = '<p class="slider-post-date">' + time[0] + '</p>' +
                '<p class="slider-post-month">' + time[1] + '</p>' +
                '<p class="slider-post-year">' + time[2] + '</p>';
            $(this).parents('.kc-entry_meta').html(html);


            let reedMoreButton = $('.post-slider .kc-read-more');
            reedMoreButton.each(function () {
                $(this).html('Читать все');
            });
        });

        /* Make image blocks equal to each other */
        let postImageBlocks = $('.kc-carousel-post .image');
        if ( ! postImageBlocks.length) {
            postImageBlocks = $('.posts-item .image')
        }
        optimizeImgSize(postImageBlocks);
    });

    function optimizeImgSize(postImageBlocks) {
        if (! postImageBlocks.length) {
            return;
        }
        let imgWidth, imgHeight, postImage;
        postImageBlocks.each(function () {
            if (! imgWidth || ! imgHeight) {
                imgWidth = parseInt($(this).width()) - 10;
                imgHeight = parseInt(imgWidth / 1.64);
            }
            postImage = $(this).find('img');
            if(imgWidth > imgHeight) {
                postImage.height(imgHeight);
                postImage.width('auto');
            } else {
                postImage.width(imgWidth);
                postImage.css('max-width', 'none');
            }
            postImage.css('max-width', 'none');
            postImage.css('overflow', 'hidden');
        });

    }
} )( jQuery );